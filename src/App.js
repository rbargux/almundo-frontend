import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View
} from 'react-native';
import { Router, Scene } from 'react-native-router-flux';
import ListaHoteles from './components/listaHoteles'
import DetalleHotel from './components/detalleHotel'

export default class App extends Component {
  render() {
    return (
      <Router>
        <Scene key="root">
          <Scene key="listaHoteles"
            component={ListaHoteles}
            title="Hoteles Disponibles"
            initial
          />
          <Scene
            key="detalleHotel"
            component={DetalleHotel}
            title="Detalle Hotel"
          />
        </Scene>
      </Router>
    );
  }
}

