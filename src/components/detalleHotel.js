'use strict'
import React, { Component } from 'react';
import {
    View,
    Text,
    FlatList,
    StyleSheet,
    Image
    //MapView
} from 'react-native';
import StarRating from 'react-native-star-rating';
import { Column as Col, Row } from 'react-native-flexbox-grid';
import MapView from 'react-native-maps';

class DetalleHotel extends Component {

    constructor(props) {
        super(props);
        this.state = {
            data: this.props.hotel,
            marker: { title: "Hotel Intercontinental", description: "El mejor lugar para descansar" }
        };
    }

    render() {
        return (
            <View style={styles.container}>
                <MapView style={styles.mapcontainer}
                    initialRegion={{
                        latitude: parseFloat(this.state.data.latitud) ,
                        longitude: parseFloat(this.state.data.longitud),
                        latitudeDelta: 0.004757,
                        longitudeDelta: 0.006866,
                    }}
                    showsUserLocation={true}
                    showsMyLocationButton={false}
                    zoomEnabled={true} >
                    <MapView.Marker
                        coordinate={{ latitude: parseFloat(this.state.data.latitud), longitude: parseFloat(this.state.data.longitud)  }}
                        title={this.state.data.titulomapa}
                        description={this.state.data.descripcionmapa}
                        image={require('../img/iconoHotel.png')}
                    />
                </MapView>
                <View style={{height:320}}>
                    <Col sm={12} style={{ backgroundColor: "white"}}>
                        <Row size={12}>
                            <Col sm={1}></Col>
                            <Col sm={5} style={{ marginVertical: 5 }}>
                                <Text style={{ fontSize: 16, textAlign: 'left', fontWeight: 'bold' }}>{this.state.data.name}</Text>
                                <StarRating disabled={false} maxStars={5} rating={this.state.data.stars} starSize={15} starColor={'#fec401'} />
                            </Col>
                            <Col sm={6} style={{ marginVertical: 5 }}>
                                <Text style={{ fontSize: 10, textAlign: 'center' }}>El precio por Noche</Text>
                                <Text style={{ fontSize: 16, fontWeight: 'bold', textAlign: 'center', color: '#fec401' }}>ARS {this.state.data.price}</Text>
                            </Col>
                        </Row>
                    </Col>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    mapcontainer: {
        width: 400,
        height: 450,
    },
});

export default DetalleHotel
