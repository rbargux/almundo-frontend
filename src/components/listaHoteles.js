'use strict'
import React, { Component } from 'react';
import {
    View,
    Text,
    FlatList,
    StyleSheet,
    Image,
    TextInput,
    TouchableHighlight
} from 'react-native';

import { Actions } from 'react-native-router-flux';
import { Column as Col, Row } from 'react-native-flexbox-grid';
import StarRating from 'react-native-star-rating';

class ListaHoteles extends Component {
    constructor(props) {
        super(props)
        this.state = {
            data: [],
            filterText: ''
        }
    }

    componentWillMount() {
        this.fetchData();
        // this.setState({ data: [{url:'http://media.expedia.com/hotels/12000000/11750000/11742600/11742514/11742514_5_b.jpg',name:'Primer Hotel',price:'4500', stars:3}
        // ,{url:'http://media.expedia.com/hotels/12000000/11750000/11742600/11742549/11742549_2_b.jpg',name:'Segundo Hotel',price:'3500', stars:5}
        //     ] });
    }

    _onChangeFilterText = (filterText) => {
        this.setState({ filterText });
    };

    async fetchData() {
        console.log("Entre");
        try {
            let response = await fetch('http://192.168.0.105:8585/api/hotels');
            let responseJson = await response.json();
            this.setState({ data: responseJson });
        } catch (error) {
            console.error(error);
        }
    }

    //#################################################


    render() {
        const filterRegex = new RegExp(String(this.state.filterText), 'i');
        const filter = (item) => (
            filterRegex.test(item.name)
        );
        const filteredData = this.state.data.filter(filter);
        return (
            <View style={{ marginTop: 20, marginHorizontal: 10, marginBottom:50 }}>
                <View>
                    <TextInput
                        style={{ height: 40, borderColor: 'gray', borderWidth: 1, backgroundColor: 'white' }}
                        onChangeText={this._onChangeFilterText}
                        placeholder="Buscar..."
                        value={this.state.filterText}
                    />
                </View>
                <FlatList
                    data={filteredData}
                    keyExtractor={(x, i) => i}
                    renderItem={({ item }) =>
                        <Col sm={12} style={{ backgroundColor: "white", marginTop: 15, elevation: 2 }}>
                            <Row size={12}>
                                <Col sm={12}>
                                    <TouchableHighlight onPress={() => Actions.detalleHotel({hotel:item}) }>
                                        <Image style={{ width: 350, height: 120, justifyContent: 'center', alignItems: 'center' }}
                                        source={{ uri: item.url }} />
                                    </TouchableHighlight>
                                </Col>
                            </Row>
                            <Row size={12}>
                                <Col sm={1}></Col>

                                <Col sm={5} style={{ marginVertical: 5 }}>
                                    <Text style={{ fontSize: 16, textAlign: 'left', fontWeight: 'bold' }}>{item.name}</Text>
                                    <StarRating disabled={false} maxStars={5} rating={item.stars} starSize={15} starColor={'#fec401'} />
                                </Col>
                                <Col sm={6} style={{ marginVertical: 5 }}>
                                    <Text style={{ fontSize: 10, textAlign: 'center' }}>El precio por Noche</Text>
                                    <Text style={{ fontSize: 16, fontWeight: 'bold', textAlign: 'center', color: '#fec401' }}>ARS {item.price}</Text>
                                    {/* <Text>{item.price}</Text> */}
                                </Col>
                            </Row>
                        </Col>
                    }
                />
            </View>


        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#f6f8fa'
    }
});

export default ListaHoteles