# Examen Ingreso Almundo (Front-End)

React Native le permite crear aplicaciones móviles usando solo JavaScript. Utiliza el mismo diseño que React, lo que le permite componer una interfaz de usuario móvil rica a partir de componentes declarativos.


## Descargar.
Clone the repo via ssh: git clone git@gitlab.com:rbargux/almundo-frontend.git


### Quick Start

1. Cambiar la ip a la que apunta el metodo fetchData, ubicado en el archivo /src/components/listaHoteles.js , por la ip del host donde este corriente el proyecto almundo-backend. Es decir cambiar la dirección que apunta esta linea `http://192.168.0.105:8585/api/hotels` por la de nuestro backend.  
2. Ejecuta el comando `npm install`.
3. Ejecuta en una consola el comando `react-native start`.
4. Ejecuta en una consola el comando `react-native run-android`.


[React Native]: https://facebook.github.io/react-native/


### Tips 
1. Limpiar Cache  `npm clean cache`.
2. si se presenta el siguiente error "com.android.dex.DexException: Multiple dex files define Landroid/support/v7/appcompat/R$anim", entonces limpiar la carpeta build, es decir:
a) `cd android`
b) `./gradlew clean`
c) `cd ..`


